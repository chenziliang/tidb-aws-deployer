variable "aws_access_key" {
  type        = "string"
  description = "Your AWS key id"
}

variable "aws_secret_key" {
  type        = "string"
  description = "Your AWS access id"
}

variable "aws_region" {
  type        = "string"
  description = "The AWS region you want to create resources"
  default     = "us-west-2"
}

variable "public_subnets" {
  type = "list"
  default = ["10.0.0.0/24"]
}

variable "private_subnets" {
  type = "list"
  default = ["10.0.1.0/24"]
}

variable "vpc_cidr_block" {
  type = "string"
  default = "10.0.0.0/16"
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  default = "true"
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  default = "true"
}

variable "tidb_instance_type_number" {
  type        = "string"
  default = "2"
}

variable "tidb_instance_type_map" {
  type = "map"

  default = {
    "1" = "t2.micro"
    "2" = "m4.2xlarge"
  }
}

variable "tikv_instance_type_number" {
  type        = "string"
  default = "2"
}

variable "tikv_instance_type_map" {
  type = "map"

  default = {
    "1" = "t2.micro"
    "2" = "m4.2xlarge"
  }
}

variable "pd_instance_type_number" {
  type        = "string"
  default = "2"
}

variable "pd_instance_type_map" {
  type = "map"

  default = {
    "1" = "t2.micro"
    "2" = "m4.2xlarge"
  }
}

variable "tidb_count" {
    type = "string"
    default = "3"
}

variable "tikv_count" {
    type = "string"
    default = "3"
}

variable "pd_count" {
    type = "string"
    default = "3"
}

variable "monitor_count" {
    type = "string"
    default = "1"
}
